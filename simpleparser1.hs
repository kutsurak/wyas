module Main where
import           Control.Monad
import           Control.Monad.Error
import           Numeric
import           System.Environment
import           System.IO
import           Text.ParserCombinators.Parsec hiding (spaces)

main :: IO ()
-- main = do
--   args <- getArgs
--   putStr (readExpr (head args))
main = do
  args <- getArgs
  case length args of
    0 -> runRepl
    1 -> evalAndPrint $ head args
    _ -> putStrLn "Program takes only 0 or 1 argument"

symbol :: Parser Char
symbol = oneOf "!$%&|*+-/:<=?>@^_~"

readExpr :: String -> ThrowsError LispVal
readExpr input = case parse parseExpr "lisp" input of
  Left err -> throwError $ Parser err
  Right val -> return val

spaces :: Parser ()
spaces = skipMany1 space

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | Float Float
             | String String
             | Bool Bool
             | Character Char

parseString :: Parser LispVal
parseString = do _ <- char '"'
                 x <- many (escaped <|> noneOf "\"")
                 _ <- char '"'
                 return $ String x

escaped :: Parser Char
escaped = do _ <- char '\\'
             esc <- oneOf "\"nrt\\"
             return $ case esc of
               '\\' -> '\\'
               'n' -> '\n'
               'r' -> '\r'
               't' -> '\t'
               '"' -> '\"'

parseAtom :: Parser LispVal
parseAtom = do first <- letter <|> symbol
               rest <- many ( letter <|> digit <|> symbol )
               let atom = first : rest
               return $ Atom atom

parseBool :: Parser LispVal
parseBool = do _ <- char '#'
               (char 't' >> return (Bool True)) <|> (char 'f' >> return (Bool False))

parseNumber :: Parser LispVal
parseNumber = parseRadixNumber <|> parseDecimal

data Radix = Decimal
           | Octal
           | Hexadecimal
           | Binary

parseRadix :: Parser Radix
parseRadix = do p <- char '#' >> oneOf "doxb"
                return $ case p of
                  'd' -> Decimal
                  'o' -> Octal
                  'x' -> Hexadecimal
                  'b' -> Binary

parseDecimal :: Parser LispVal
parseDecimal = liftM (Number . read) $ many1 digit

parseOctal :: Parser LispVal
parseOctal = liftM (Number . fst . head . readOct) $ many1 octDigit

parseHex :: Parser LispVal
parseHex = liftM (Number . fst . head . readHex) $ many1 hexDigit

parseBin :: Parser LispVal
parseBin = liftM (Number . fst . head . readBin) $ many1 binDigit

binDigit :: Parser Char
binDigit = satisfy (`elem` "01") <?> "binary digit"

readBin :: (Eq a, Num a) => ReadS a
readBin [] = []
readBin s = readBin' 0 s
  where readBin' n ('0':xs) = readBin' (2*n) xs
        readBin' n ('1':xs) = readBin' (2*n + 1) xs
        readBin' n xs = [(n, xs)]

parseRadixNumber :: Parser LispVal
parseRadixNumber = do r <- parseRadix
                      case r of
                        Decimal     -> parseDecimal
                        Octal       -> parseOctal
                        Hexadecimal -> parseHex
                        Binary      -> parseBin

parseCharacter :: Parser LispVal
parseCharacter = do _ <- try $ char '#' >> char '\\'
                    x <- characterName <|>
                         anyChar
                    return ( Character x )

characterName :: Parser Char
characterName = (string "space" >> return ' ') <|>
                (string "newline" >> return '\n')

parseFloat :: Parser LispVal
parseFloat = liftM (Float . fst . head . readFloat) buildFloatString

buildFloatString :: Parser String
buildFloatString = try fullString <|>
                   try integralString <|>
                   fractionalString

integralString :: Parser String
integralString =  do
  intPart <- many1 digit
  _       <- char '.'
  return $ intPart ++ "."

fractionalString :: Parser String
fractionalString = do
  _        <- char '.'
  fracPart <- many1 digit
  return $ "0." ++ fracPart

fullString :: Parser String
fullString =  do
  intPart  <- many1 digit
  _        <- char '.'
  fracPart <- many1 digit
  return $ intPart ++ "." ++ fracPart


parseExpr :: Parser LispVal
parseExpr = parseAtom
            <|> parseString
            <|> try parseFloat
            <|> parseNumber
            <|> parseCharacter
            <|> parseBool
            <|> parseQuoted
            <|> do _ <- char '('
                   x <- try parseList <|> parseDottedList
                   _ <- char ')'
                   return x

parseList :: Parser LispVal
parseList = liftM List $ sepBy parseExpr spaces

parseDottedList :: Parser LispVal
parseDottedList = do
  lHead <- endBy parseExpr spaces
  lTail <- char '.' >> spaces >> parseExpr
  return $ DottedList lHead lTail

parseQuoted :: Parser LispVal
parseQuoted = do
  _ <- char '\''
  x <- parseExpr
  return $ List [Atom "quote", x]

showVal :: LispVal -> String
showVal (String contents) = "\"" ++ contents ++ "\""
showVal (Atom name) = name
showVal (Number contents) = show contents
showVal (Float contents) = show contents
showVal (Bool True) = "#t"
showVal (Bool False) = "#f"
showVal (List contents) = "(" ++ unwordsList contents ++ ")"
showVal (DottedList listHead listTail) = "(" ++ unwordsList listHead ++ " . " ++ showVal listTail ++ ")"
showVal (Character contents) = "#\\" ++ escapeChar contents

unwordsList :: [LispVal] -> String
unwordsList = unwords . map showVal

escapeChar :: Char -> String
escapeChar ' ' = "space"
escapeChar '\n' = "newline"
escapeChar c = [c]

instance Show LispVal where show = showVal

eval :: LispVal -> ThrowsError LispVal
eval val@(String _) = return val
eval val@(Number _) = return val
eval val@(Float _) = return val
eval val@(Bool _) = return val
eval val@(Character _) = return val
eval (List [Atom "quote", val]) = return val
eval (List [Atom "if", prd, conseq, alt]) =
  do result <- eval prd
     case result of
       Bool False -> eval alt
       _ -> eval conseq
eval form@(List (Atom "cond": clauses)) =
      if null clauses
      then throwError $ BadSpecialForm "no true clause in cond expression: " form
      else case head clauses of
      List [Atom "else", expr] -> eval expr
      List [test, expr] -> eval $ List [Atom "if", test, expr,
                                        List (Atom "cond" : tail clauses)]
      _ -> throwError $ BadSpecialForm "ill-formed cond expression: " form
eval (List (Atom func : args)) = mapM eval args >>= apply func
eval badForm = throwError $ BadSpecialForm "Unrecognized special form" badForm


apply :: String -> [LispVal] -> ThrowsError LispVal
apply func args = maybe (throwError $ NotFunction "Unrecongized primitive function args" func)
                  ($ args)
                  (lookup func primitives)

primitives :: [(String, [LispVal] -> ThrowsError LispVal)]
primitives = [("+", numericBinop (+)),
              ("-", numericBinop (-)),
              ("*", numericBinop (*)),
              ("/", numericBinop div),
              ("mod", numericBinop mod),
              ("quotient", numericBinop quot),
              ("remainder", numericBinop rem),
              ("symbol?", unaryOp symbolp),
              ("number?", unaryOp numberp),
              ("string?", unaryOp stringp),
              ("bool?", unaryOp boolp),
              ("list?", unaryOp listp),
              ("symbol->string", unaryOp symbolToString),
              ("string->symbol", unaryOp stringToSymbol),
              ("=", numBoolBinop (==)),
              ("<", numBoolBinop (<)),
              (">", numBoolBinop (>)),
              ("/=", numBoolBinop (/=)),
              (">=", numBoolBinop (>=)),
              ("<=", numBoolBinop (<=)),
              ("&&", boolBoolBinop (&&)),
              ("||", boolBoolBinop (||)),
              ("string=?", strBoolBinop (==)),
              ("string<?", strBoolBinop (<)),
              ("string>?", strBoolBinop (>)),
              ("string<=?", strBoolBinop (<=)),
              ("string>=?", strBoolBinop (>=)),
              ("car", car),
              ("cdr", cdr),
              ("cons", cons),
              ("eq?", eqv),
              ("eqv", eqv)
             ]

numericBinop :: (Integer -> Integer -> Integer) -> [LispVal] -> ThrowsError LispVal
numericBinop _ [] = throwError $ NumArgs 2 []
numericBinop _ singleVal@[_] = throwError $ NumArgs 2 singleVal
numericBinop op params = mapM unpackNum params >>= return . Number . foldl1 op

unpackNum :: LispVal -> ThrowsError Integer
unpackNum (Number n) = return n
unpackNum val = throwError $ TypeMismatch "Expected int" val

unaryOp :: (LispVal -> LispVal) -> [LispVal] -> ThrowsError LispVal
unaryOp _ [] = throwError $ NumArgs 2 []
unaryOp f [v] = return $ f v
unaryOp _ vals = throwError $ NumArgs 2 vals

symbolp, numberp, stringp, boolp, listp ::  LispVal -> LispVal

symbolp (Atom _) = Bool True
symbolp _ = Bool False

numberp (Number _) = Bool True
numberp (Float _) = Bool True
numberp _ = Bool False

stringp (String _) = Bool True
stringp _ = Bool False

boolp (Bool _) = Bool True
boolp _ = Bool False

listp (List _) = Bool True
listp (DottedList _ _) = Bool True
listp _ = Bool False

symbolToString :: LispVal -> LispVal
symbolToString (Atom x) = String x
symbolToString _ = String "Error"

stringToSymbol :: LispVal -> LispVal
stringToSymbol (String x) = Atom x
stringToSymbol _ = String "Error"

data LispError = NumArgs Integer [LispVal]
               | TypeMismatch String LispVal
               | Parser ParseError
               | BadSpecialForm String LispVal
               | NotFunction String String
               | UnboundVar String String
               | Default String

showError :: LispError -> String
showError (UnboundVar message varname)  = message ++ ": " ++ varname
showError (BadSpecialForm message form) = message ++ ": " ++ show form
showError (NotFunction message func)    = message ++ ": " ++ show func
showError (NumArgs expected found)      = "Expected " ++ show expected
                                          ++ " args; found values: " ++ unwordsList found
showError (TypeMismatch expected found) = "Invalid type: expected " ++ show expected
                                          ++ " args; found values: " ++ show found
showError (Parser parseErr)             = "Parse error at " ++ show parseErr

instance Show LispError where show = showError

instance Error LispError where
  noMsg = Default "An error has occured"
  strMsg = Default

type ThrowsError = Either LispError

trapError action = catchError action (return . show)

extractValue :: ThrowsError a -> a
extractValue (Right val) = val

boolBinop :: (LispVal -> ThrowsError a) -> (a -> a -> Bool) -> [LispVal] -> ThrowsError LispVal
boolBinop unpacker op args = if length args /= 2
                             then throwError $ NumArgs 2 args
                             else do left <- unpacker $ args !! 0
                                     right <- unpacker $ args !! 1
                                     return $ Bool $ left `op` right

numBoolBinop  = boolBinop unpackNum
strBoolBinop  = boolBinop unpackStr
boolBoolBinop = boolBinop unpackBool

unpackStr :: LispVal -> ThrowsError String
unpackStr (String s) = return s
unpackStr val = throwError $ TypeMismatch "string" val

unpackBool :: LispVal -> ThrowsError Bool
unpackBool (Bool b) = return b
unpackBool val = throwError $ TypeMismatch "boolean" val

car :: [LispVal] -> ThrowsError LispVal
car [List (x:_)]         = return x
car [DottedList (x:_) _] = return x
car [badArg]             = throwError $ TypeMismatch "pair" badArg
car badArgList           = throwError $ NumArgs 1 badArgList

cdr :: [LispVal] -> ThrowsError LispVal
cdr [List (_:xs)]         = return $ List xs
cdr [DottedList [_] x]    = return x
cdr [DottedList (_:xs) x] = return $ DottedList xs x
cdr [badArg]              = throwError $ TypeMismatch "pair" badArg
cdr badArgList           = throwError $ NumArgs 1 badArgList

cons :: [LispVal] -> ThrowsError LispVal
cons [x1, List []] = return $ List [x1]
cons [x, List xs] = return $ List (x:xs)
cons [x, DottedList xs y] = return $ DottedList (x:xs) y
cons [x1, x2] = return $ DottedList [x1] x2
cons badArgList = throwError $ NumArgs 2 badArgList

eqv :: [LispVal] -> ThrowsError LispVal
eqv [(Bool arg1), (Bool arg2)]             = return $ Bool $ arg1 == arg2
eqv [(Number arg1), (Number arg2)]         = return $ Bool $ arg1 == arg2
eqv [(String arg1), (String arg2)]         = return $ Bool $ arg1 == arg2
eqv [(Float arg1), (Float arg2)]           = return $ Bool $ arg1 == arg2
eqv [(Atom arg1), (Atom arg2)]             = return $ Bool $ arg1 == arg2
eqv [(DottedList xs x), (DottedList ys y)] = eqv [List $ xs ++ [x], List $ ys ++ [y]]
eqv [(List arg1), (List arg2)]             = return $ Bool $ (length arg1 == length arg2) &&
                                             (all eqvPair $ zip arg1 arg2)
  where eqvPair (x1, x2) = case eqv [x1, x2] of
          Left _ -> False
          Right (Bool val) -> val
eqv [_, _]                                 = return $ Bool False
eqv badArgList                             = throwError $ NumArgs 2 badArgList

-- REPL
flushStr :: String -> IO ()
flushStr str = putStr str >> hFlush stdout

readPrompt :: String -> IO String
readPrompt prompt = flushStr prompt >> getLine

evalString :: String -> IO String
evalString expr = return $ extractValue $ trapError (liftM show $ readExpr expr >>= eval)

evalAndPrint :: String -> IO ()
evalAndPrint expr = evalString expr >>= putStrLn

until_ :: Monad m => (a -> Bool) -> m a -> (a -> m ()) -> m ()
until_ pred prompt action = do
  result <- prompt
  if pred result
    then return ()
    else action result >> until_ pred prompt action

runRepl :: IO ()
runRepl = until_ (== "quit") ( readPrompt "Lisp>>> ") evalAndPrint




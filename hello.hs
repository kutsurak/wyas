module Main where
-- import System.Environment
import System.IO

-- main :: IO ()
-- main = do args <- getArgs
--           let arg0 = read (args !! 0) :: Int
--               arg1 = read (args !! 1) :: Int
--           putStrLn  ("Sum: " ++  show ( arg0 + arg1 ) )

main :: IO ()
main = do putStr "Name? "
          hFlush stdout
          line <- getLine
          putStrLn ( "Hello, " ++ line )
